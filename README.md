Calculates factorial of a number (rounded down if not an integer)

# Install

```
npm install js-fact-lib
yarn add js-fact-lib
```

# Usage

```
const fact = require("../index.js");
console.log(fact(4));
```

# Contribute

Requeriments:
* Node 16.15.1 tested
