function innerFact(n) {
    if (isNaN(n)) {
        return 1;
    }
    if (n <= 1) {
        return 1;
    }
    return n * innerFact(n - 1);
}

function fact(n) {
    return innerFact(Math.floor(n));
}

module.exports = fact;
