const fact = require("../index.js");

describe("fact()", () => {
    test("Should return 1 for a non-number", () => {
        expect(fact("a")).toEqual(1);
    });
});

describe("fact()", () => {
    test("Should return 1 for negative numbers", () => {
        expect(fact(-1)).toEqual(1);
    });
});

describe("fact()", () => {
    test("Should calculate the factorial", () => {
        expect(fact(3)).toEqual(6);
    });
});
